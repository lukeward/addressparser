﻿using AddressParser.Model;
using System;
using System.Collections;
using System.Collections.Generic;

namespace AddressParserTests
{
    class TownsComparer : Comparer<GeoData>
    {
        public override int Compare(GeoData x, GeoData y)
        {
            int comparison = x.Town.CompareTo(y.Town);

            if (comparison != 0) return comparison;

            comparison = x.County.CompareTo(y.County);

            if (comparison != 0) return comparison;

            comparison = x.Country.CompareTo(y.Country);

            if (comparison != 0) return comparison;

            return x._id.CompareTo(y._id);
        }
    }
}