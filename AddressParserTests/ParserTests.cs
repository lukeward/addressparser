﻿using AddressParser.Model;
using NUnit.Framework;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Bson;
using AddressParser.Repository;
using AddressParserTest.FakeMongoContext;
using AddressParser.Service;
namespace AddressParserTests
{
    public class ParserTests
    {
        /*
        Import input csv file to mongo
        Scan mongo towns for imported towns, move any match to out field
        Scan mongo counties for imported counties, move any match to out field
        */
        private Address[] _addresses;

        [SetUp]
        public void SetUp()
        {
            _addresses[0] = new Address
            {
                CompanyName = "ABC Ltd.",
                AddressLine1 = "16 Balshaw House Gardens",
                AddressLine2 = "Euxton",
                AddressLine3 = "",
                AddressLine4 = "Chorley",
                AddressLine5 = "",
                AddressLine6 = "Lancashire",
                AddressLine7 = "GB",
                AddressLine8 = "PR7 6QG"
            };

            _addresses[1] = new Address
            {
                CompanyName = "RTM Ltd.",
                AddressLine1 = "Unit 6b",
                AddressLine2 = "Balfour Court",
                AddressLine3 = "Balfour Road",
                AddressLine4 = "Leyland",
                AddressLine5 = "Preston",
                AddressLine6 = "Lancashire",
                AddressLine7 = "GB",
                AddressLine8 = "PR25 2TF"
            };
        }

        [Test]
        public void Should_find_Chorley_in_address_0_and_copy_to_out_town_field()
        {
            InsertTestTowns();

            var parserService = new ParserService();

            parserService.ParseTown(_addresses[0], GetTestTownData());

            Assert.AreEqual("Chorley", _addresses[0].Out_Town);
        }

        [Test]
        public void Should_find_Leyland_in_address_and_copy_to_out_town_field()
        {
            InsertTestTowns();

            var parserService = new ParserService();

            parserService.ParseTown(_addresses[1], GetTestTownData());

            Assert.AreEqual("Chorley", _addresses[1].Out_Town);
        }

        [Test]
        public void Should_return_list_of_all_UK_towns()
        {
            InsertTestTowns();

            var townsAndCountiesRepository = new AddressDataRepository(new FakeParserContext());

            List<GeoData> listOfTowns = townsAndCountiesRepository.GetAllTowns();
            
            CollectionAssert.AreEqual(GetTestTownData(), listOfTowns, new TownsComparer());
        }

        private static void InsertTestTowns()
        {
            var client = new MongoClient();
            var db = client.GetDatabase("testTownsCounties");

            db.DropCollection("towns");

            var collection = db.GetCollection<BsonDocument>("towns");

            foreach(var doc in GetTestTownData())
            {
                collection.InsertOne(doc.ToBsonDocument());
            }
        }

        private static List<GeoData> GetTestTownData()
        {
            return new List<GeoData>
            {
                new GeoData
                {
                    _id= new ObjectId("507f191e810c19729de860ea"),Town ="Chorley",County="Lancashire",Country="England"
                },
                new GeoData
                {
                     _id= new ObjectId("407f191e810c19729de860ea"),Town="Folkestone",County="Kent",Country="England"
                },
                new GeoData
                {
                    _id= new ObjectId("307f191e810c19729de860ea"),Town ="Dorchester",County="Dorset",Country="England"
                }
            };
        }
    }
}
