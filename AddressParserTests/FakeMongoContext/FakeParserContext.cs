﻿using AddressParser.Model;
using AddressParser.MongoContext;
using MongoDB.Driver;

namespace AddressParserTest.FakeMongoContext
{
    public class FakeParserContext:IParserContext
    {
        // IoC container?
        private static readonly IMongoClient _client;
        private static readonly IMongoDatabase _database;

        static FakeParserContext()
        {
            _client = new MongoClient("mongodb://localhost:27017");
            _database = _client.GetDatabase("testTownsCounties");
        }
        
        public IMongoCollection<GeoData> TownsAndCounties
        {
            get { return _database.GetCollection<GeoData>("towns"); }
        }
    }
}