﻿using AddressParser.Model;
using AddressParser.MongoContext;
using MongoDB.Bson;
using System.Collections.Generic;
using MongoDB.Driver;

namespace AddressParser.Repository
{
    public class AddressDataRepository
    {
        private IParserContext _parserContext;

        public AddressDataRepository(IParserContext parserContext)
        {
            _parserContext = parserContext;
        }

        public List<GeoData> GetAllTowns()
        {
            return _parserContext.TownsAndCounties.Find(new BsonDocument()).ToList();
        }
    }
}