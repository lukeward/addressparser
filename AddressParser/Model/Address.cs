﻿namespace AddressParser.Model
{
    public class Address
    {
        public string CompanyName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string AddressLine4 { get; set; }
        public string AddressLine5 { get; set; }
        public string AddressLine6 { get; set; }
        public string AddressLine7 { get; set; }
        public string AddressLine8 { get; set; }
        public string AddressLine9 { get; set; }
        public string Out_CompanyName { get; set; }
        public string Out_AddressLine1 { get; set; }
        public string Out_AddressLine2 { get; set; }
        public string Out_AddressLine3 { get; set; }
        public string Out_AddressLine4 { get; set; }
        public string Out_Town { get; set; }
        public string Out_County { get; set; }
        public string Out_Postcode { get; set; }
    }
}
