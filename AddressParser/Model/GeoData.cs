﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AddressParser.Model
{
    public class GeoData
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("Id")]
        public ObjectId _id { get; set; }
        public string Town { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
    }
}
