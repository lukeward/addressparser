﻿using AddressParser.Model;
using MongoDB.Driver;

namespace AddressParser.MongoContext
{
    public interface IParserContext
    {
        IMongoCollection<GeoData> TownsAndCounties { get; }
    }
}