﻿using AddressParser.Model;
using MongoDB.Driver;

namespace AddressParser.MongoContext
{
    public class ParserContext : IParserContext
    {
        // IoC container?
        private static readonly IMongoClient _client;
        private static readonly IMongoDatabase _database;

        static ParserContext()
        {
            _client = new MongoClient("mongodb://localhost:27017");
            _database = _client.GetDatabase("towns_counties");
        }
        
        public IMongoCollection<GeoData> TownsAndCounties
        {
            get { return _database.GetCollection<GeoData>("town_counties"); }
        }
    }
}